## Plugin Dependencies

- [CommandAPI](https://github.com/JorelAli/CommandAPI)
- [mcMMO](https://www.spigotmc.org/resources/64348/)
- [Vault](https://www.spigotmc.org/resources/34315/)
- [LuckPerms](https://www.spigotmc.org/resources/28140/)
- [DeluxeCombat](https://www.spigotmc.org/resources/63970/)
- [Parties](https://bitbucket.org/ancientrealms/parties/)
