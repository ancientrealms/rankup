package it.ancientrealms.rankup;

import java.io.File;
import java.math.BigDecimal;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.electronwill.nightconfig.core.file.FileConfig;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import it.ancientrealms.Parties;
import it.ancientrealms.api.PartyManager;
import it.ancientrealms.rankup.command.AoeCommand;
import it.ancientrealms.rankup.command.BoostexpCommand;
import it.ancientrealms.rankup.command.CraftCommand;
import it.ancientrealms.rankup.command.EditconfigCommand;
import it.ancientrealms.rankup.command.InvisibilityCommand;
import it.ancientrealms.rankup.command.RankupCommand;
import it.ancientrealms.rankup.listener.RankUpListener;
import it.ancientrealms.rankup.manager.BoostExpManager;
import it.ancientrealms.rankup.manager.CommandCooldownManager;
import it.ancientrealms.rankup.manager.RankUpManager;
import it.ancientrealms.rankup.runnable.BoostExpRunnable;
import it.ancientrealms.rankup.runnable.CommandCooldownTask;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.milkbowl.vault.economy.Economy;
import nl.marido.deluxecombat.DeluxeCombat;
import nl.marido.deluxecombat.api.DeluxeCombatAPI;

public final class RankUp extends JavaPlugin
{
    private static RankUp instance;
    private RankUpManager rankUpManager;
    private CommandCooldownManager commandCooldownManager;
    private DeluxeCombatAPI combatManager;
    private PartyManager partyManager;
    private BoostExpManager boostExpManager;
    private Economy economy;
    private FileConfig config;
    private FileConfig ranks;
    private FileConfig messages;
    private FileConfig players;

    @Override
    public void onEnable()
    {
        instance = this;

        this.rankUpManager = new RankUpManager();
        this.commandCooldownManager = new CommandCooldownManager(this);
        this.boostExpManager = new BoostExpManager();

        this.combatManager = DeluxeCombat.getAPI();

        this.economy = this.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
        this.partyManager = Parties.Companion.getINSTANCE().getPartyManager();

        this.config = this.initConfig("config.toml");
        this.ranks = this.initConfig("ranks.json");
        this.messages = this.initConfig("messages.toml");
        this.players = this.initConfig("players.json");

        this.getServer().getPluginManager().registerEvents(new RankUpListener(), this);

        new CommandCooldownTask().runTaskTimer(this, 0, 5);
        new BoostExpRunnable().runTaskTimer(this, 0, 5);

        AoeCommand.register();
        BoostexpCommand.register();
        CraftCommand.register();
        EditconfigCommand.register();
        InvisibilityCommand.register();
        RankupCommand.register();
    }

    @Override
    public void onDisable()
    {
    }

    public static RankUp getInstance()
    {
        return instance;
    }

    public RankUpManager getRankUpManager()
    {
        return this.rankUpManager;
    }

    public CommandCooldownManager getCommandCooldownManager()
    {
        return this.commandCooldownManager;
    }

    public BoostExpManager getBoostExpManager()
    {
        return this.boostExpManager;
    }

    public Economy getEconomy()
    {
        return this.economy;
    }

    public PartyManager getPartyManager()
    {
        return this.partyManager;
    }

    public LuckPerms getLuckPermsApi()
    {
        return LuckPermsProvider.get();
    }

    public FileConfig getPluginConfig()
    {
        return this.config;
    }

    public FileConfig getRanks()
    {
        return this.ranks;
    }

    public String getMessage(String path)
    {
        return (String) this.messages.get(path);
    }

    public FileConfig getPlayers()
    {
        return this.players;
    }

    public int getIntBalance(Player player)
    {
        return new BigDecimal(String.valueOf(this.getEconomy().getBalance(player))).intValue();
    }

    public void canExecuteCommand(Player player, String command) throws WrapperCommandSyntaxException
    {
        if (this.commandCooldownManager.hasCooldown(player.getUniqueId(), command))
        {
            final long cd = this.commandCooldownManager.getRemainingTime(player.getUniqueId(), command);
            CommandAPI.fail(this.getMessage("errors.command_wait_cooldown").formatted(cd));
        }

        if (this.combatManager.isInCombat(player))
        {
            CommandAPI.fail(this.getMessage("errors.cannot_use_command_during_combattag"));
        }
    }

    private FileConfig initConfig(String filename)
    {
        this.saveResource(filename, false);

        final FileConfig config = FileConfig.builder(new File(this.getDataFolder(), filename))
                .autoreload()
                .autosave()
                .sync()
                .build();
        config.load();

        return config;
    }
}
