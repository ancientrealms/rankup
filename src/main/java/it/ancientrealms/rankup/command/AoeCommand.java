package it.ancientrealms.rankup.command;

import java.time.Duration;
import java.util.Optional;
import java.util.UUID;

import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.EntityType;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.electronwill.nightconfig.core.Config;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import it.ancientrealms.model.Party;
import it.ancientrealms.rankup.RankUp;

public final class AoeCommand
{
    private static final RankUp plugin = RankUp.getInstance();

    public static void register()
    {
        new CommandAPICommand("aoe")
                .withPermission("rankup.command.aoe")
                .withArguments(new GreedyStringArgument("aoe").replaceSuggestions(info -> {
                    return ((Config) plugin.getPluginConfig().get("aoe_command_effects")).entrySet()
                            .stream()
                            .map(e -> ((Config) e.getValue()).get("text"))
                            .toArray(String[]::new);
                }))
                .executesPlayer((player, args) -> {
                    plugin.canExecuteCommand(player, "aoe");

                    final String potiontext = (String) args[0];
                    final Optional<String> potionname = ((Config) plugin.getPluginConfig().get("aoe_command_effects")).entrySet()
                            .stream()
                            .filter(e -> ((Config) e.getValue()).get("text").equals(potiontext))
                            .map(e -> e.getKey())
                            .findAny();

                    if (potionname.isEmpty())
                    {
                        CommandAPI.fail(plugin.getMessage("errors.effect_not_found"));
                    }

                    final PotionEffectType potioneffecttype = PotionEffectType.getByName(potionname.get());
                    final UUID playeruuid = player.getUniqueId();

                    plugin.getCommandCooldownManager().add(playeruuid, "aoe");

                    final String fmt = "aoe_command_effects." + potionname.get();

                    final int duration = (boolean) plugin.getPluginConfig().get(fmt + ".only_in_cloud")
                            ? 2
                            : (int) Duration.parse("PT" + (String) plugin.getPluginConfig().get(fmt + ".duration")).toSeconds();
                    final int amplifier = plugin.getPluginConfig().getInt(fmt + ".amplifier");
                    final PotionEffect potion = new PotionEffect(potioneffecttype, 20 * duration, amplifier);
                    final AreaEffectCloud effectcloud = (AreaEffectCloud) player.getWorld().spawnEntity(player.getLocation(), EntityType.AREA_EFFECT_CLOUD);

                    final Party party = plugin.getPartyManager().getPlayerParty(playeruuid);
                    if (party == null)
                    {
                        final NamespacedKey key = new NamespacedKey(plugin, "aoeplayer");
                        effectcloud.getPersistentDataContainer().set(key, PersistentDataType.INTEGER, -1);
                    }
                    else
                    {
                        final UUID leaderuuid = party.getLeader();
                        final NamespacedKey key = new NamespacedKey(plugin, "aoeparty");
                        effectcloud.getPersistentDataContainer().set(key, PersistentDataType.STRING, leaderuuid.toString());
                    }

                    effectcloud.addCustomEffect(potion, true);
                    player.playSound(player.getLocation(), Sound.ENTITY_SPLASH_POTION_BREAK, 1, 1);
                })
                .register();
    }
}
