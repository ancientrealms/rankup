package it.ancientrealms.rankup.command;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataType;

import dev.jorel.commandapi.CommandAPICommand;
import it.ancientrealms.model.Party;
import it.ancientrealms.rankup.RankUp;

public final class BoostexpCommand
{
    private static final RankUp plugin = RankUp.getInstance();

    public static void register()
    {
        new CommandAPICommand("boostexp")
                .withPermission("rankup.command.boostexp")
                .executesPlayer((player, args) -> {
                    plugin.canExecuteCommand(player, "boostexp");

                    plugin.getCommandCooldownManager().add(player.getUniqueId(), "boostexp");

                    final String duration = (String) plugin.getPluginConfig().get("command.boostexp_duration");
                    final long seconds = Duration.parse("PT" + duration).toSeconds();
                    final Instant expiretime = Instant.now().plusSeconds(seconds);

                    plugin.getBoostExpManager().add(player.getUniqueId(), expiretime);

                    final NamespacedKey bostexpkey = new NamespacedKey(plugin, "boostexp");
                    player.getPersistentDataContainer().set(bostexpkey, PersistentDataType.INTEGER, -1);

                    player.sendMessage(plugin.getMessage("info.boostexp_activated"));

                    final Party party = plugin.getPartyManager().getPlayerParty(player.getUniqueId());

                    if (party == null)
                    {
                        return;
                    }

                    for (UUID memberid : party.getPlayerList())
                    {
                        final Player member = plugin.getServer().getPlayer(memberid);

                        if (member == null)
                        {
                            continue;
                        }

                        plugin.getBoostExpManager().add(memberid, expiretime);
                        member.getPersistentDataContainer().set(bostexpkey, PersistentDataType.INTEGER, -1);
                        member.sendMessage(plugin.getMessage("info.boostexp_activated_by").formatted(player.getName()));
                    }
                })
                .register();
    }
}
