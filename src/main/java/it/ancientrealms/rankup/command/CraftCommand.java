package it.ancientrealms.rankup.command;

import com.github.stefvanschie.inventoryframework.gui.type.CraftingTableGui;

import dev.jorel.commandapi.CommandAPICommand;

public final class CraftCommand
{
    public static void register()
    {
        new CommandAPICommand("craft")
                .withPermission("rankup.command.craft")
                .executesPlayer((player, args) -> {
                    new CraftingTableGui("").show(player);
                })
                .register();
    }
}
