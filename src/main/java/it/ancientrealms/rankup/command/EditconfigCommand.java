package it.ancientrealms.rankup.command;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ItemStackArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import it.ancientrealms.rankup.RankUp;

public final class EditconfigCommand
{
    private static final RankUp plugin = RankUp.getInstance();

    public static void register()
    {
        command("guiBackgroundItem", "gui_background_item");
        command("rankLockedItem", "rank_locked_item");
        command("rankNotAvailableItem", "rank_not_available_item");
        command("rankUnlockedItem", "rank_unlocked_item");
    }

    private static void command(String commandName, String configPath)
    {
        new CommandAPICommand("rankup")
                .withPermission("rankup.command.editconfig")
                .withArguments(new LiteralArgument("editconfig"))
                .withArguments(new LiteralArgument(commandName))
                .withArguments(new ItemStackArgument("itemstack"))
                .executes((sender, args) -> {
                    final Material material = ((ItemStack) args[0]).getType();

                    plugin.getPluginConfig().set(configPath, material.toString());
                    sender.sendMessage(plugin.getMessage("info.config_updated"));
                })
                .register();
    }
}
