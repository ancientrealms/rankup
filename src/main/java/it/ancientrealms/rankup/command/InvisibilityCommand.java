package it.ancientrealms.rankup.command;

import java.time.Duration;
import java.util.UUID;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import dev.jorel.commandapi.CommandAPICommand;
import it.ancientrealms.model.Party;
import it.ancientrealms.rankup.RankUp;

public final class InvisibilityCommand
{
    private static final RankUp plugin = RankUp.getInstance();

    public static void register()
    {
        new CommandAPICommand("invisibility")
                .withPermission("rankup.command.invisibility")
                .executesPlayer((player, args) -> {
                    plugin.canExecuteCommand(player, "invisibility");

                    plugin.getCommandCooldownManager().add(player.getUniqueId(), "invisibility");

                    final String fmt = (String) plugin.getPluginConfig().get("command.invisibility_duration");
                    final int duration = (int) Duration.parse("PT" + fmt).toSeconds();

                    final PotionEffect potion = new PotionEffect(PotionEffectType.INVISIBILITY, 20 * duration, 0);

                    potion.apply(player);
                    player.playSound(player.getLocation(), Sound.ENTITY_SPLASH_POTION_BREAK, 1, 1);

                    final Party party = plugin.getPartyManager().getPlayerParty(player.getUniqueId());

                    if (party == null)
                    {
                        return;
                    }

                    for (UUID memberid : party.getPlayerList())
                    {
                        final Player member = plugin.getServer().getPlayer(memberid);

                        if (member == null)
                        {
                            continue;
                        }

                        potion.apply(member);
                        member.playSound(member.getLocation(), Sound.ENTITY_SPLASH_POTION_BREAK, 1, 1);
                    }
                })
                .register();
    }
}
