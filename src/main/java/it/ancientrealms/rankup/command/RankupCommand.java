package it.ancientrealms.rankup.command;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import com.github.stefvanschie.inventoryframework.adventuresupport.ComponentHolder;
import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane.Priority;
import com.github.stefvanschie.inventoryframework.pane.PatternPane;
import com.github.stefvanschie.inventoryframework.pane.util.Pattern;
import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.api.exceptions.McMMOPlayerNotFoundException;

import dev.jorel.commandapi.CommandAPICommand;
import it.ancientrealms.rankup.RankUp;
import it.ancientrealms.rankup.config.Players;
import it.ancientrealms.rankup.config.Ranks;
import it.ancientrealms.rankup.util.Util;
import net.kyori.adventure.text.Component;

public final class RankupCommand
{
    private static final RankUp plugin = RankUp.getInstance();

    public static void register()
    {
        new CommandAPICommand("rankup")
                .withPermission("rankup.command")
                .executesPlayer((player, args) -> {
                    final ChestGui gui = new ChestGui(6,
                            ComponentHolder.of(Util.baseComponent(plugin.getMessage("gui.title"))));

                    gui.setOnGlobalClick(event -> event.setCancelled(true));

                    final OutlinePane background = new OutlinePane(0, 0, 9, 6, Priority.LOWEST);
                    background.addItem(new GuiItem(new ItemStack(Material.getMaterial((String) plugin.getPluginConfig().get("gui.background_item")))));
                    background.setRepeat(true);

                    gui.addPane(background);

                    final Pattern pattern = new Pattern(
                            "000000000",
                            "010203040",
                            "000000000",
                            "000000000",
                            "050607080",
                            "000000000");
                    final PatternPane rankspane = new PatternPane(0, 0, 9, 6, pattern);

                    for (int rankid : IntStream.rangeClosed(1, 8).toArray())
                    {
                        final ItemStack itemstack = new ItemStack(Material.getMaterial((String) plugin.getPluginConfig().get("gui.rank_locked_item")));
                        final ItemMeta itemmeta = itemstack.getItemMeta();
                        final Ranks rank = Ranks.get(rankid);

                        final PersistentDataContainer data = itemmeta.getPersistentDataContainer();
                        final NamespacedKey mcmmokey = new NamespacedKey(plugin, "mcmmo");
                        final NamespacedKey moneykey = new NamespacedKey(plugin, "money");
                        final NamespacedKey lockedkey = new NamespacedKey(plugin, "locked");

                        final List<Component> lore = new ArrayList<>();

                        if (rank.exists())
                        {
                            lore.add(Component.empty());

                            data.set(mcmmokey, PersistentDataType.INTEGER, rank.mcmmo());
                            data.set(moneykey, PersistentDataType.INTEGER, rank.money());

                            itemmeta.displayName(Util.baseComponent(rank.name()));

                            lore.add(Util.baseComponent(plugin.getMessage("gui.item_rewards_title")));
                            lore.add(Component.empty());

                            for (String s : rank.lore())
                            {
                                lore.add(Util.baseComponent(s));
                            }

                            lore.add(Component.empty());

                            final Players prank = Players.get(player);
                            final int currentrank = prank.getRank();

                            if (currentrank >= rankid)
                            {
                                lore.add(Util.baseComponent(plugin.getMessage("gui.already_owned_rank")));
                                itemstack.setType(Material.getMaterial((String) plugin.getPluginConfig().get("gui.rank_unlocked_item")));
                                data.set(lockedkey, PersistentDataType.INTEGER, 1);
                            }
                            else
                            {
                                lore.add(Util.baseComponent(plugin.getMessage("gui.item_requirements_title")));
                                lore.add(Component.empty());

                                final int playermoney = plugin.getIntBalance(player);
                                final int rankmoney = data.get(moneykey, PersistentDataType.INTEGER);
                                final String moneymsg = playermoney < rankmoney ? "gui.money_no" : "gui.money_yes";
                                final String moneyfmt = plugin.getMessage(moneymsg).formatted(playermoney, rankmoney);

                                lore.add(Util.baseComponent(moneyfmt));

                                int playermcmmo;
                                try
                                {
                                    playermcmmo = ExperienceAPI.getPowerLevel(player);
                                }
                                catch (McMMOPlayerNotFoundException ex)
                                {
                                    player.sendMessage(plugin.getMessage("errors.mcmmo_not_loaded_yet"));
                                    return;
                                }
                                final int rankmcmmo = data.get(mcmmokey, PersistentDataType.INTEGER);

                                if (!(playermoney > rankmoney && playermcmmo > rankmcmmo))
                                {
                                    data.set(lockedkey, PersistentDataType.INTEGER, 1);
                                }

                                final String mcmmomsg = playermcmmo < rankmcmmo ? "gui.mcmmo_no" : "gui.mcmmo_yes";
                                final String mcmmofmt = plugin.getMessage(mcmmomsg).formatted(playermcmmo, rankmcmmo);
                                lore.add(Util.baseComponent(mcmmofmt));

                                if (currentrank != (rankid - 1))
                                {
                                    lore.add(Component.empty());
                                    lore.add(Util.baseComponent(plugin.getMessage("gui.need_previous_rank")));
                                    data.set(lockedkey, PersistentDataType.INTEGER, 1);
                                }
                            }
                        }
                        else
                        {
                            itemstack.setType(Material.getMaterial((String) plugin.getPluginConfig().get("gui.rank_not_available_item")));
                            itemmeta.displayName(Util.baseComponent(plugin.getMessage("gui.item_not_available_title")));
                            data.set(lockedkey, PersistentDataType.INTEGER, 1);
                        }

                        itemmeta.lore(lore);
                        itemstack.setItemMeta(itemmeta);

                        rankspane.bindItem(String.valueOf(rankid).charAt(0), new GuiItem(itemstack, event -> {
                            if (data.has(lockedkey, PersistentDataType.INTEGER))
                            {
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1, 1);
                                return;
                            }

                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1, 1);
                            plugin.getRankUpManager().updatePlayerRank(player, rankid);
                            event.getInventory().close();
                        }));
                    }

                    gui.addPane(rankspane);
                    gui.show(player);
                })
                .register();
    }
}
