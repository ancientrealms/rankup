package it.ancientrealms.rankup.config;

import org.bukkit.entity.Player;

import it.ancientrealms.rankup.RankUp;

public final class Players
{
    private static RankUp plugin = RankUp.getInstance();
    private String uuid;

    private Players(String uuid)
    {
        this.uuid = uuid;
    }

    public static Players get(Player player)
    {
        return new Players(player.getUniqueId().toString());
    }

    public static Players create(Player player)
    {
        final String uuid = player.getUniqueId().toString();
        plugin.getPlayers().add("players." + uuid + ".new_rank_notified", false);
        plugin.getPlayers().add("players." + uuid + ".current_rank", 0);

        return new Players(uuid);
    }

    public boolean getRankNotified()
    {
        return plugin.getPlayers().get("players." + uuid + ".new_rank_notified");
    }

    public void setRankNotified(boolean bool)
    {
        plugin.getPlayers().set("players." + uuid + ".new_rank_notified", bool);
    }

    public int getRank()
    {
        return plugin.getPlayers().getInt("players." + uuid + ".current_rank");
    }

    public void setRank(int rankId)
    {
        plugin.getPlayers().set("players." + uuid + ".current_rank", rankId);
    }
}
