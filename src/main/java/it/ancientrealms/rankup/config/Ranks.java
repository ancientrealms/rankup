package it.ancientrealms.rankup.config;

import java.util.List;

import it.ancientrealms.rankup.RankUp;

public final class Ranks
{
    private static final RankUp plugin = RankUp.getInstance();
    private int rankId;

    private Ranks(int rankId)
    {
        this.rankId = rankId;
    }

    private final <T> T get(final String path)
    {
        return plugin.getRanks().get("ranks." + this.rankId + "." + path);
    }

    public static Ranks get(int number)
    {
        return new Ranks(number);
    }

    public boolean exists()
    {
        return this.get("enabled") != null ? true : false;
    }

    public boolean enabled()
    {
        return this.get("enabled");
    }

    public String name()
    {
        return this.get("name");
    }

    public int mcmmo()
    {
        return this.get("requirements.mcmmo");
    }

    public int money()
    {
        return this.get("requirements.money");
    }

    public boolean cooldownReset()
    {
        return this.get("rewards.cooldown_reset");
    }

    public int grindstoneExpModifier()
    {
        return this.get("rewards.grindstone_exp_modifier");
    }

    public String groupName()
    {
        return this.get("rewards.group_name");
    }

    public List<String> lore()
    {
        final List<String> lore = this.get("lore");
        return lore;
    }
}
