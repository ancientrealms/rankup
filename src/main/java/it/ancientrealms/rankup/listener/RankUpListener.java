package it.ancientrealms.rankup.listener;

import java.util.UUID;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.AreaEffectCloudApplyEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.events.experience.McMMOPlayerLevelUpEvent;

import it.ancientrealms.model.Party;
import it.ancientrealms.rankup.RankUp;
import it.ancientrealms.rankup.config.Players;
import it.ancientrealms.rankup.config.Ranks;
import it.ancientrealms.rankup.util.Util;
import nl.marido.deluxecombat.tasks.UsageLimitationTask;

public final class RankUpListener implements Listener
{
    private final RankUp plugin = RankUp.getInstance();

    @EventHandler
    public void playerJoin(PlayerJoinEvent event)
    {
        plugin.getRankUpManager().registerPlayer(event.getPlayer());
    }

    @EventHandler
    public void mcmmoPlayerLevelUp(McMMOPlayerLevelUpEvent event)
    {
        final Player player = event.getPlayer();
        final Players prank = Players.get(player);
        final boolean newranknotified = prank.getRankNotified();

        if (newranknotified)
        {
            return;
        }

        final int powerlevel = ExperienceAPI.getPowerLevel(player);
        final int playermoney = plugin.getIntBalance(player);
        final int currentrank = prank.getRank();

        for (int rankid = 1; rankid < 8; rankid++)
        {
            if (currentrank >= rankid)
            {
                continue;
            }

            final Ranks rank = Ranks.get(rankid);
            final int rankmcmmo = rank.mcmmo();
            final int rankmoney = rank.money();

            if (powerlevel < rankmcmmo || playermoney < rankmoney)
            {
                continue;
            }

            prank.setRankNotified(true);
            player.sendMessage(Util.baseComponent(plugin.getMessage("info.new_rank_available")));
        }
    }

    @EventHandler
    public void grindstonePlayerExpChange(PlayerExpChangeEvent event)
    {
        if (!event.getPlayer().getOpenInventory().getType().equals(InventoryType.GRINDSTONE))
        {
            return;
        }

        final Players prank = Players.get(event.getPlayer());
        final int currentrank = prank.getRank();

        if (currentrank == 0)
        {
            return;
        }

        final int expmodifier = Ranks.get(currentrank).grindstoneExpModifier();

        if (expmodifier == 0)
        {
            return;
        }

        event.setAmount(event.getAmount() * expmodifier);
    }

    @EventHandler
    public void areaEffectCloudCommand(AreaEffectCloudApplyEvent event)
    {
        final NamespacedKey playerkey = new NamespacedKey(plugin, "aoeplayer");
        final NamespacedKey partykey = new NamespacedKey(plugin, "aoeparty");
        final PersistentDataContainer data = event.getEntity().getPersistentDataContainer();

        final boolean hasplayerkey = data.has(playerkey, PersistentDataType.INTEGER);
        final boolean haspartykey = data.has(partykey, PersistentDataType.STRING);

        if (hasplayerkey || haspartykey)
        {
            event.getAffectedEntities().removeIf(e -> !(e instanceof Player));
        }

        if (!haspartykey)
        {
            return;
        }

        final UUID leaderuuid = UUID.fromString(data.get(partykey, PersistentDataType.STRING));
        final Party party = plugin.getPartyManager().getPlayerParty(leaderuuid);

        if (party == null)
        {
            return;
        }

        event.getAffectedEntities().removeIf(e -> {
            return !party.getPlayerList().contains(((Player) e).getUniqueId());
        });
    }

    @EventHandler
    public void boostExpPlayerExpChange(PlayerExpChangeEvent event)
    {
        final NamespacedKey key = new NamespacedKey(plugin, "boostexp");
        final PersistentDataContainer data = event.getPlayer().getPersistentDataContainer();

        if (!data.has(key, PersistentDataType.INTEGER))
        {
            return;
        }

        final int modifier = plugin.getPluginConfig().getInt("command_boostexp_modifier");
        event.setAmount(event.getAmount() * modifier);
    }

    @EventHandler
    public void boostExpPlayerJoin(PlayerJoinEvent event)
    {
        final NamespacedKey boostexpkey = new NamespacedKey(plugin, "boostexp");
        final Player player = event.getPlayer();
        final boolean hasboostexp = player.getPersistentDataContainer().has(boostexpkey, PersistentDataType.INTEGER);

        if (!hasboostexp)
        {
            return;
        }

        final boolean boostexpover = !plugin.getBoostExpManager().getPlayers().containsKey(player.getUniqueId());

        if (!boostexpover)
        {
            return;
        }

        player.getPersistentDataContainer().remove(boostexpkey);
        player.sendMessage(plugin.getMessage("info.boostexp_expired"));
    }

    @EventHandler
    public void entityDeathResetKillerCooldown(EntityDeathEvent event)
    {
        final LivingEntity entity = event.getEntity();

        if (!(entity instanceof Player))
        {
            return;
        }

        final Player victim = (Player) event.getEntity();
        final Player killer = victim.getKiller();

        if (killer == null)
        {
            return;
        }

        final Players prank = Players.get(killer);
        final int currentrank = prank.getRank();

        if (currentrank == 0)
        {
            return;
        }

        if (!Ranks.get(currentrank).cooldownReset())
        {
            return;
        }

        UsageLimitationTask.getGoldApples().remove(killer);
        UsageLimitationTask.getEnderpearls().remove(killer);

        killer.sendMessage(plugin.getMessage("info.cooldown_cleared").formatted(victim.getName()));
    }
}
