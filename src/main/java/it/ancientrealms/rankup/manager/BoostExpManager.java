package it.ancientrealms.rankup.manager;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class BoostExpManager
{
    private final Map<UUID, Instant> players = new HashMap<>();

    public void add(UUID playerId, Instant instant)
    {
        this.players.put(playerId, instant);
    }

    public Map<UUID, Instant> getPlayers()
    {
        return this.players;
    }
}
