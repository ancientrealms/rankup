package it.ancientrealms.rankup.manager;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.UUID;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import it.ancientrealms.rankup.RankUp;
import it.ancientrealms.rankup.object.CommandCooldown;

public final class CommandCooldownManager
{
    private final RankUp plugin;
    private final Multimap<UUID, CommandCooldown> cooldowns = ArrayListMultimap.create();

    public CommandCooldownManager(RankUp plugin)
    {
        this.plugin = plugin;
    }

    public void add(UUID player, String commandName)
    {
        final String fmt = (String) plugin.getPluginConfig().get("command.cooldowns." + commandName);
        final Instant time = Instant.now().plusSeconds(Duration.parse("PT" + fmt).toSeconds());
        this.cooldowns.put(player, new CommandCooldown(commandName, time));
    }

    public void clear(UUID player)
    {
        this.cooldowns.removeAll(player);
    }

    public boolean hasCooldown(UUID player, String commandName)
    {
        final Collection<CommandCooldown> cd = this.cooldowns.get(player);
        return cd.stream().anyMatch(c -> c.getCommandName().equals(commandName));
    }

    public long getRemainingTime(UUID player, String commandName)
    {
        final Instant cd = this.cooldowns.get(player)
                .stream()
                .filter(c -> c.getCommandName().equals(commandName))
                .findFirst()
                .get()
                .getCommandTime();
        return Instant.now().until(cd, ChronoUnit.SECONDS);
    }

    public Multimap<UUID, CommandCooldown> getCooldowns()
    {
        return this.cooldowns;
    }
}
