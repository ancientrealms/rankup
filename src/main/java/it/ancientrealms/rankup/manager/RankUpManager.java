package it.ancientrealms.rankup.manager;

import java.util.UUID;

import org.bukkit.entity.Player;

import it.ancientrealms.rankup.RankUp;
import it.ancientrealms.rankup.config.Players;
import it.ancientrealms.rankup.config.Ranks;
import net.luckperms.api.model.data.DataMutateResult;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.types.InheritanceNode;

public final class RankUpManager
{
    private final RankUp plugin = RankUp.getInstance();

    public void registerPlayer(Player player)
    {
        Players.create(player);
    }

    public DataMutateResult updatePlayerRank(Player player, int rankId)
    {
        final UUID uuid = player.getUniqueId();
        final User user = plugin.getLuckPermsApi().getUserManager().getUser(uuid);
        final Ranks rank = Ranks.get(rankId);

        // TODO: add logging
        if (!rank.exists())
        {
            return DataMutateResult.FAIL;
        }

        final String groupname = rank.groupName();
        final Group group = plugin.getLuckPermsApi().getGroupManager().getGroup(groupname);
        final InheritanceNode node = InheritanceNode.builder(group).build();

        final DataMutateResult result = user.data().add(node);

        if (result.equals(DataMutateResult.FAIL))
        {
            plugin.getLogger().severe(plugin.getMessage("errors.unable_to_set_group").formatted(player.getName(), groupname));
            return result;
        }

        plugin.getLuckPermsApi().getUserManager().saveUser(user);

        final int money = rank.money();
        plugin.getEconomy().withdrawPlayer(player, money);

        final Players prank = Players.get(player);
        prank.setRankNotified(false);
        prank.setRank(rankId);

        final String rankname = rank.name();
        player.sendMessage(plugin.getMessage("info.upgrade_success").formatted(rankname));

        return DataMutateResult.SUCCESS;
    }
}
