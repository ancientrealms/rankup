package it.ancientrealms.rankup.object;

import java.time.Instant;

public class CommandCooldown
{
    private final String commandName;
    private final Instant time;

    public CommandCooldown(String commandName, Instant time)
    {
        this.commandName = commandName;
        this.time = time;
    }

    public String getCommandName()
    {
        return this.commandName;
    }

    public Instant getCommandTime()
    {
        return this.time;
    }
}
