package it.ancientrealms.rankup.runnable;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import it.ancientrealms.rankup.RankUp;

public final class BoostExpRunnable extends BukkitRunnable
{
    private final RankUp plugin = RankUp.getInstance();

    @Override
    public void run()
    {
        final Instant now = Instant.now();

        for (Map.Entry<UUID, Instant> e : plugin.getBoostExpManager().getPlayers().entrySet())
        {
            if (now.isBefore(e.getValue()))
            {
                continue;
            }

            final Player player = plugin.getServer().getPlayer(e.getKey());

            if (player == null)
            {
                continue;
            }

            player.sendMessage(plugin.getMessage("info.boostexp_expired"));
            player.getPersistentDataContainer().remove(new NamespacedKey(plugin, "boostexp"));
        }

        plugin.getBoostExpManager().getPlayers().values().removeIf(t -> now.isAfter(t));
    }
}
