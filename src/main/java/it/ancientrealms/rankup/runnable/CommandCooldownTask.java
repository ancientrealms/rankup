package it.ancientrealms.rankup.runnable;

import java.time.Instant;

import org.bukkit.scheduler.BukkitRunnable;

import it.ancientrealms.rankup.RankUp;

public final class CommandCooldownTask extends BukkitRunnable
{
    private final RankUp plugin = RankUp.getInstance();

    @Override
    public void run()
    {
        plugin.getCommandCooldownManager()
                .getCooldowns()
                .values()
                .removeIf(c -> Instant.now().isAfter(c.getCommandTime()));
    }
}
