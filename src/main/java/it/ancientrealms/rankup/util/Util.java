package it.ancientrealms.rankup.util;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextDecoration;
import net.kyori.adventure.text.minimessage.MiniMessage;

public final class Util
{
    public static Component baseComponent(String string)
    {
        return Component.empty().decoration(TextDecoration.ITALIC, false).append(MiniMessage.get().parse(string));
    }
}
